## Intallation

# Setup Database
1. buat sebuah database dan setup config di /server/.env.development 

```
...

DB_NAME=ifabulacms
DB_USER=root
DB_PASSWORD=ar011011
DB_HOST=0.0.0.0
DB_DIALECT=mysql

...

```

2. Jalankan migrasi & Seeder user untuk login
```
npx sequelize-cli db:migrate --env "development"

npx sequelize-cli db:seed:all
```

3. jalankan aplikasi fe vite react

```
cd client
client> npm run dev

```

4. jalankan aplikasi be node

```
cd server
server> npm run dev

```

5. user login 
```
username : admin
password : 1234

```
