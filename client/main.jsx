import {StrictMode} from 'react'
import ReactDOM from 'react-dom/client'

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

library.add(fas, far, fab)

// Import our custom CSS
import './src/assets/scss/styles.scss'
// Import all of Bootstrap's JS
import * as bootstrap from 'bootstrap'

import App from './App'

import { Provider } from 'react-redux'
import store from './src/redux/stores'

ReactDOM.createRoot(document.getElementById('root')).render(
  <StrictMode>
    <Provider store={store}>
    <App />
    </Provider>
  </StrictMode>
)
