import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useMemo, useState } from 'react';
import DataTable from 'react-data-table-component';
import { Bars } from  'react-loader-spinner'
import { useDispatch } from 'react-redux';

const customStyles = {
 table: {
		style: {
			borderCollapse: 'collapse'
		},
	},
	subHeader: {
		style: {
			justifyContent: 'space-between',
			padding: '0px'
		}
	},
 head: {
		style: {
			fontSize: '0.875rem',
			fontWeight: 'bold',
		},
	},
	headRow: {
		style: {
			borderWidth: '1px 0px 1px 1px',
			borderColor: '#dee2e6',
			borderStyle: 'solid',
   minHeight: '2.5rem', // override the row height
		},
	},
	headCells: {
		style: {
   padding: '0.5rem 0.5rem',
   borderColor: '#dee2e6',
   borderWidth:'0px 1px 0px 0px',
   borderStyle : 'solid'
		},
		draggingStyle: {
			cursor: 'move',
		},
	},
 rows: {
     style: {
         minHeight: '2.5rem', // override the row height
         borderStyle: 'solid',
         borderColor: '#dee2e6',
         borderWidth: '0px 0px 1px 1px'
     },
 },
 cells: {
     style: {
      verticalAlign: 'top',
      padding: '0.5rem 0.5rem',
      borderColor: '#dee2e6',
      borderWidth:'0px 1px 0px 0px',
      borderStyle : 'solid'
     },
 },
};

const Table = ({data,columns,loading,totalRows,subHeader, setPage, setPerPage, setKeyword, setOrder}) => {
	const dispatch = useDispatch()
	const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
 const [rows, setRows] = useState('');
 const [search, setSearch] = useState('');
	
	const handleChangePage = async (page,totalRows) => {
		setPage(page)
	}

	const handleChangePerPage = async (perPage,page) => {
		setPerPage(perPage)
		setPage(page)
	}
		
	const handleSort = async (column, sortDirection) => {
		if(column?.sortField)
		setOrder([column.sortField,sortDirection]);
		else
		setOrder([])
	};

 const subHeaderComponentMemo = useMemo(() => {
		const handleClear = () => {
			if (search) {
				setSearch('');
				setResetPaginationToggle(!resetPaginationToggle);
			}
		};

		const handleSearch = () => {
			setKeyword(search)
			setResetPaginationToggle(!resetPaginationToggle);
		}

		return (
			<>
				<div className='d-flex'>
					{subHeader}
				</div>
				<div className='d-flex'>
					<div className="input-group mb-3">
								<input type="text" className="form-control" placeholder="Search ..."
								onChange={e => setSearch(e.target.value)}
								value={search}
								style={{'borderRight':'0px'}}
								/>
								{search && <button onClick={handleClear} className="btn btn-outline-secondary" type="button" style={{'borderColor':'#ced4da','borderWidth':'1px 0px'}}><FontAwesomeIcon icon="fa-solid fa-xmark" /></button>}
								<button className="btn btn-secondary" type="button" onClick={handleSearch}>Search</button>
						</div>
				</div>
			</>
		);
	}, [search, resetPaginationToggle]);

	useEffect(()=>{
			let start = 1;
			const newData = data.map((row)=>{
				return {...row, no:++start}
			})

			setRows(newData)
			
	},[data])

 return <DataTable 
 data={rows} 
 columns={columns} 
 theme={'solarized'} 
 customStyles={customStyles}
 onSort={handleSort}
 paginationTotalRows={totalRows}
 onChangePage={handleChangePage}
 onChangeRowsPerPage={handleChangePerPage}
 sortServer
 pagination
 paginationServer
 progressPending={loading}
 progressComponent={<Bars
    height="40"
    width="40"
    color="#dee2e6"
    ariaLabel="bars-loading"
    wrapperStyle={{}}
    wrapperclassName=""
    visible={true}
  />}
  paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
  subHeader
  subHeaderComponent={subHeaderComponentMemo}
 />
}

export default Table
