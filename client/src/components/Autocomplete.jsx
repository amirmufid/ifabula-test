import { useEffect, useState } from "react"
import axios from "axios"

const Autocomplete = ({data,onKeyUp,text,setValueSuggestion,formik,name,defaultValue,...props}) => {
 const [suggestions, setSuggestions] = useState([])
 const [textValue, setTextValue] = useState('')
 const [val,setVal] = useState('');
 useEffect(() => {
  if(textValue?.length > 0 && data?.count > 0){
   setSuggestions(data.rows)
  } 
  if(textValue?.length < 1){
   setSuggestions([])
  }
 }, [data])

 useEffect(() => {
  if(defaultValue){
    setTextValue(defaultValue)
  }
 }, [defaultValue])
 
 

 const onChangeHandler = (textValue) => {
  onKeyUp(textValue)
  setTextValue(textValue)
}
const onSuggestHandler = (value,textValue) => {
 
  formik.setFieldValue(`${name}`,value)
  setTimeout(() => formik.setFieldTouched(`${name}`, true))
  
  setTextValue(textValue)
  setVal(value)
  setSuggestions([])
}
  
  return(
  <>
   <input type="hidden" name={name} className="form-control" {...formik.getFieldProps(`${name}`)} />
   <input 
   name={`${name}_text`}
   className={`form-control ${formik.errors[name] && formik.touched[name] ? `is-invalid` : formik.values[name] && 'is-valid'}`}
   type="text" id="perusahaan" autoComplete="off" onChange={e => onChangeHandler(e.target.value)} value={textValue} />
   {formik.errors[name] && formik.touched[name] && (<i className="invalid-feedback">{formik.errors[name]}</i>)}
   <ul className="list-group">
   {suggestions && suggestions.map((suggestion,i)=> (<li key={i} className="list-group-item" onClick={() => onSuggestHandler(setValueSuggestion(suggestion),text(suggestion))} style={{cursor:'pointer'}} >{text(suggestion)}</li>))}
   </ul>
  </>
  )
}

export default Autocomplete
