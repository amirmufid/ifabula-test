import axios from "axios";
import { CSVLink } from "react-csv";
import moment from "moment/moment";
import { useEffect, useRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export const getToken = () => {
  const loginStorage = localStorage.getItem("login") != null ? JSON.parse(localStorage.getItem("login")) : null
  const token = loginStorage?.accessToken?.token || null;
  return token;
 };
 
 const API = axios.create({ 
  baseURL: env.API_URL,
  timeout: 5000
 });

const CSVDownloadButton = ({filename,url}) => {
 const [loading,setLoading] = useState(false)
 const [data,setData] = useState(null)
 const csvInstance = useRef();

 useEffect(() => {
   if (data && csvInstance.current && csvInstance.current.link) {
     setTimeout(() => {
       csvInstance.current.link.click();
       setData(null);
     });
   }
 }, [data]);

 const getData = (event, done) => {
   setLoading(true)
   API.get(`/${url}`,
    {headers: {Authorization: `Bearer ${getToken()}`}
   }).then((csvData)=>{
     
     setLoading(false)
     setData({
         data: csvData.data,
         filename: `${moment().format()}-${filename}`
     })
   }).catch(() => {
     setLoading(false)
   });
 }

 return (
 <>
   <button className="btn btn-sm btn-success" onClick={getData}>
     <FontAwesomeIcon icon="fa-solid fa-file-csv" className="me-2"/>
     {loading ? `Loading csv..`: `Export CSV`}
   </button>
   {data && (<CSVLink 
     data={data.data}
     filename={data.filename}
     enclosingCharacter={`"`}
     ref={csvInstance}
   />)}
 </>
 )
}
export default CSVDownloadButton
