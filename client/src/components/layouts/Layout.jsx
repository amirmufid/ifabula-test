import { Suspense } from "react";
import { Await, useLoaderData, Navigate, Outlet, Link  } from "react-router-dom";
import { useAuth } from "../../../hooks/useAuth";
import ErrorPage from "../ErrorPage";

export const Root = () => {
  const { userPromise } = useLoaderData()

  return (
   <Suspense>
    <Await
     resolve={userPromise}
     errorElement={<ErrorPage />}
     children={(user)=>(
      <Outlet/>
     )}
    >
    </Await>
   </Suspense>
  );
};

export const Auth = () => {
  const { auth } = useAuth();
  if (!auth) {
    return <Navigate to="/login" />;
  }

  return (
      <Outlet />
  )
};

export const NoAuth = () => {
  const { auth } = useAuth();
  
  if (auth) {
    return <Navigate to="/" />;
  }

  return (
      <Outlet />
  )
};
