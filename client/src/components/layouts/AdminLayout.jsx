import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Link, Outlet } from "react-router-dom"

import { useAuth } from "../../../hooks/useAuth";
import { useLocalStorage } from "../../../hooks/useLocalStorage.jsx";

export const AdminLayout = () => {
 const [toggleNav, setToggleNav] = useLocalStorage("sidebar-toggle",false);
 
 useEffect(() => {
    if(toggleNav){
     document.body.classList.add('sb-sidenav-toggled');
    }
 },[toggleNav])
 
 const handleToggleNav = () => {
  document.body.classList.toggle('sb-sidenav-toggled');
  setToggleNav(document.body.classList.contains('sb-sidenav-toggled'));
 }
 const { logout } = useAuth()
 return (
  <div>
      <nav className="sb-topnav navbar navbar-expand navbar-dark bg-dark">
          <Link className="navbar-brand ps-3" tp="/">Ifabula CMS</Link>
          <button
              className="btn btn-link btn-sm order-1 order-lg-0 me-4 me-lg-0"
              id="sidebarToggle"
              type="button"
              onClick={handleToggleNav}
              >
              <FontAwesomeIcon icon="fa-solid fa-bars" />
          </button>
          <div
              className="d-none d-md-inline-block form-inline ms-auto me-0 me-md-3 my-2 my-md-0">
              <button type="button" className="btn btn-light" onClick={logout}> Logout</button>
          </div>
      </nav>
      <div id="layoutSidenav">
          <div id="layoutSidenav_nav">
              <nav className="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                  <div className="sb-sidenav-menu">
                      <div className="nav">
                          <Link className="nav-link" to="/">
                              <div className="sb-nav-link-icon">
                                  <FontAwesomeIcon icon="fa-solid fa-tachometer" />
                              </div>
                              Dashboard
                          </Link>
                          
                          <div className="sb-sidenav-menu-heading">Data Master</div>
                          <Link className="nav-link" to="/barang">
                              <div className="sb-nav-link-icon">
                              <FontAwesomeIcon icon="fa-solid fa-boxes-stacked" />
                              </div>
                              Barang
                          </Link>
                          <Link className="nav-link" to="/perusahaan">
                              <div className="sb-nav-link-icon">
                              <FontAwesomeIcon icon="fa-solid fa-building" />
                              </div>
                              Perusahaan
                          </Link>
                          
                          <div className="sb-sidenav-menu-heading">Data Transaksi</div>
                          <Link className="nav-link" to="/transaksi">
                              <div className="sb-nav-link-icon">
                              <FontAwesomeIcon icon="fa-solid fa-sack-dollar" />
                              </div>
                              Transaksi
                          </Link>
                      </div>
                  </div>
                  <div className="sb-sidenav-footer">
                      <div className="small">Logged in as:</div>
                      Start Bootstrap
                  </div>
              </nav>
          </div>
          <div id="layoutSidenav_content">
              <main>
                  <div className="container-fluid px-4 mt-4">
                      <Outlet/>
                  </div>
              </main>
              <footer className="py-4 bg-light mt-auto">
                  <div className="container-fluid px-4">
                      <div className="d-flex align-items-center justify-content-between small">
                          <div className="text-muted">Copyright &copy; Your Website 2021</div>
                          <div>
                              <a href="#">Privacy Policy</a>
                              &middot;
                              <a href="#">Terms &amp; Conditions</a>
                          </div>
                      </div>
                  </div>
              </footer>
          </div>
      </div>
  </div>  
 )
};
