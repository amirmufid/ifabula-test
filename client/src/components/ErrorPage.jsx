import { Link, isRouteErrorResponse, useRouteError } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const ErrorPage = () => {
    const error = useRouteError()
    let message = `Something went wrong`;
    let errorCode = 500;

    if (isRouteErrorResponse(error)) {
        errorCode = error.status

        if (error.status === 404) {
            message = `This page doesn't exist!`
        }

        if (error.status === 401) {
            message = `You aren't authorized to see this`
        }

        if (error.status === 503) {
            message = `Looks like our API is down`
        }

        if (error.status === 418) {
            message = `🫖`
        }
    }    
    
    return (
    <div id="layoutError">
        <div id="layoutError_content">
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6">
                            <div className="text-center mt-4">
                                <FontAwesomeIcon icon="fa-solid fa-triangle-exclamation" className="fa-8x"/>
                                <h1 className="display-1">{errorCode}</h1>
                                <p className="lead">{message}</p>
                                <Link to="/">
                                    <FontAwesomeIcon icon="fa-solid fa-arrow-left" className="me-1" />
                                    Return to Dashboard
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutError_footer">
            <footer className="py-4 bg-light mt-auto">
                <div className="container-fluid px-4">
                    <div className="d-flex align-items-center justify-content-between small">
                        <div className="text-muted">Copyright &copy; Your Website 2021</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    )
};

export default ErrorPage
