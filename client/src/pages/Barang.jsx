import { useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { format as currency } from "currency-formatter"
import { Button, Modal } from "react-bootstrap"

import { useGetBarangQuery, useStoreBarangMutation, useUpdateBarangMutation, useDeleteBarangMutation  } from "../redux/services/BarangApi"

import Table from "../components/Table"
import CSVDownloadButton from "../components/CSVDownloadButton";

export const Barang = () => {
  const [showConfirmDeleteModal, setshowConfirmDeleteModal] = useState(false)
  const [showFormModal, setshowFormModal] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null)
  const [perPage,setPerPage] = useState(10)
  const [page,setPage] = useState(1)
  const [order,setOrder] = useState( [] )
  const [keyword,setKeyword] = useState('')
  
  const {isError,isSuccess,isLoading,data:barang} = useGetBarangQuery({
    per_page : perPage,
    page,
    order,
    keyword,
  });
  
  const [storeBarang,{isSuccess:storeIsSuccess}] = useStoreBarangMutation();
  const [updateBarang,{isSuccess:updateIsSuccess}] = useUpdateBarangMutation();
  const [deleteBarang,{isSuccess:deleteIsSuccess}] = useDeleteBarangMutation();
  
  useEffect(() => {
    if(storeIsSuccess || updateIsSuccess || deleteIsSuccess){
      handleCloseFormModal()
      handleCloseConfirmDelete()
    } 
  }, [storeIsSuccess,updateIsSuccess,deleteIsSuccess])
  
  const formik = useFormik({
    initialValues: {
      id: '',
      nama: '',
      harga: '',
      stock: '',
    },
    validationSchema: Yup.object().shape({
      nama: Yup.string()
      .required(),
      harga: Yup.number()
      .min(0)
      .required(),
      stock: Yup.number()
      .min(0)
      .required(),
    }),
    onSubmit: values => {
      if(!values?.id || values?.id == '' || values?.id == null){
        storeBarang({
          nama : values.nama,
          harga : values.harga,
          stock : values.stock
        })
      } else {
        updateBarang({
          id : values.id,
          nama : values.nama,
          harga : values.harga,
          stock : values.stock,
        })
      }
    }
  });
  
  const handleCloseConfirmDelete = () => {
    setSelectedRow(null)
    setshowConfirmDeleteModal(false)
  }
  const handleShowConfirmDeleteModal = (row) => {
    setSelectedRow(row)
    setshowConfirmDeleteModal(true)
  }
  const handleDelete = () =>{
    deleteBarang({id:selectedRow.id})
  }
  
  const handleCloseFormModal = () => {
    setSelectedRow(null)
    setshowFormModal(false)
    formik.resetForm()
  }
  const handleShowFormModal = (row=null) => {
    setSelectedRow(row)
    setshowFormModal(true)
    if(row){
      Object.keys(row)?.map((key) =>{
        formik.setFieldValue(`${key}`,row[key])
        setTimeout(() => formik.setFieldTouched(`${key}`, true))
      });
    }
  }

  const renderTable = () => {
    const totalRows = barang?.data?.count || 0
    const dataColumns = [
      {
        name: 'No.',
        width: "5%",      
        center:true,
        cell:(row,index) => ((page-1) * perPage)+(index+1)
      },
      {
          name: 'Nama',
          selector: row => row.nama,
          sortable: true,
          sortField: 'nama'
      },
      {
          name: 'Harga',
          center:true,
          sortable: true,
          sortField: 'harga',
          selector: row => currency(row.harga,{code:'IDR'}),
      },
      {
          name: 'Stock',
          selector: row => row.stock,
          center:true,
          sortable: true,
          sortField: 'stock'
      },
      {
        center:true,
        cell:(row) => (
          <div className="btn-group " role="group" aria-label="Basic outlined example">
            <button type="button" onClick={()=>handleShowFormModal(row)} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon="fa-regular fa-pen-to-square" /></button>
            <button type="button" onClick={()=>handleShowConfirmDeleteModal(row)} className="btn btn-sm btn-outline-danger"><FontAwesomeIcon icon="fa-regular fa-trash-can" /></button>
          </div>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
    ];
    return <Table 
      columns={dataColumns} 
      data={barang?.data?.rows || []} 
      totalRows={totalRows} 
      loading={isLoading} 
      setPerPage={setPerPage}
      setPage={setPage}
      setKeyword={setKeyword}
      setOrder={setOrder}
      subHeader={<button type="button" onClick={()=>handleShowFormModal()} className="btn btn-secondary"><FontAwesomeIcon icon="fa-solid fa-plus" /> Add New Data</button>}
    />
  }

  return (
    <>
    <div>
        <h1><FontAwesomeIcon icon="fa-solid fa-boxes-stacked"/> Barang</h1>
        <div className="card my-4">
            <div className="card-header d-flex justify-content-between">
              <div style={{lineHeight:'2rem'}}>
              <FontAwesomeIcon icon="fa-solid fa-table" className="me-1"/>
              Data Barang
              </div>
              <div className="">
                <CSVDownloadButton filename={`barang`} url="barang/download/csv"/>
              </div>
            </div>
            <div className="card-body">
                {renderTable()}
            </div>
        </div>

        <Modal show={showConfirmDeleteModal} onHide={handleCloseConfirmDelete} backdrop="static" keyboard={false}>
          <Modal.Header closeButton>
            <Modal.Title>Confirm Delete</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure want to delete <b>{selectedRow?.nama}</b> ?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseConfirmDelete}>
              Close
            </Button>
            <Button variant="danger" onClick={handleDelete}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={showFormModal} onHide={handleCloseFormModal} backdrop="static" keyboard={false}>
          <form onSubmit={formik.handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>Barang</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <>
                  <div className="mb-3">
                    <label htmlFor="nama" className="form-label">Nama</label>
                    <input
                      name="nama"
                      className={`form-control ${formik.errors.nama && formik.touched.nama ? `is-invalid` : formik.values.nama && 'is-valid'}`}
                      id="nama"
                      type="text"
                      placeholder="Nama Barang"
                      {...formik.getFieldProps('nama')}
                      />
                      {formik.errors.nama && formik.touched.nama && (<i className="invalid-feedback">{formik.errors.nama}</i>)}
                  </div>
                  <div className="row">
                    <div className="col">
                      <label htmlFor="harga" className="form-label">Harga</label>
                      <input
                        name="harga"
                        className={`form-control ${formik.errors.harga && formik.touched.harga ? `is-invalid` : formik.values.harga && 'is-valid'}`}
                        id="harga"
                        type="text"
                        placeholder="Harga Barang"
                        {...formik.getFieldProps('harga')}
                        />
                        {formik.errors.harga && formik.touched.harga && (<i className="invalid-feedback">{formik.errors.harga}</i>)}
                    </div>
                    <div className="col">
                      <label htmlFor="stock" className="form-label">Stock</label>
                      <input
                        name="stock"
                        className={`form-control ${formik.errors.stock && formik.touched.stock ? `is-invalid` : formik.values.stock && 'is-valid'}`}
                        id="stock"
                        type="text"
                        placeholder="Stock Barang"
                        {...formik.getFieldProps('stock')}
                        />
                        {formik.errors.stock && formik.touched.stock && (<i className="invalid-feedback">{formik.errors.stock}</i>)}
                    </div>
                  </div>
                </>
            </Modal.Body>
            <Modal.Footer>
              <button onClick={handleCloseFormModal} type="button" className="btn btn-secondary">Close</button>
              <button type="submit" className="btn btn-primary" disabled={!(formik.isValid)}>Save</button>
            </Modal.Footer>
          </form>
        </Modal>
    </div>
    </>
  )
}
