import { useEffect, useRef, useState } from "react"
import { useDispatch, useSelector } from "react-redux";
import { useFormik } from "formik";
import * as Yup from "yup"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { format as currency } from "currency-formatter"
import { Button, Modal } from "react-bootstrap"

import { useGetPerusahaanQuery, useStorePerusahaanMutation, useUpdatePerusahaanMutation, useDeletePerusahaanMutation  } from "../redux/services/PerusahaanApi"

import Table from "../components/Table"
import CSVDownloadButton from "../components/CSVDownloadButton";
export const Perusahaan = () => {
  const [showConfirmDeleteModal, setshowConfirmDeleteModal] = useState(false)
  const [showFormModal, setshowFormModal] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null)
  const [perPage,setPerPage] = useState(10)
  const [page,setPage] = useState(1)
  const [order,setOrder] = useState( [] )
  const [keyword,setKeyword] = useState('')
  
  const {isError,isLoading,isSuccess,data:perusahaan} = useGetPerusahaanQuery({
    per_page : perPage,
    page,
    order,
    keyword,
  });
  const [storePerusahaan,{isSuccess:storeIsSuccess}] = useStorePerusahaanMutation();
  const [updatePerusahaan,{isSuccess:updateIsSuccess}] = useUpdatePerusahaanMutation();
  const [deletePerusahaan,{isSuccess:deleteIsSuccess}] = useDeletePerusahaanMutation();
  
  
  useEffect(() => {
    if(storeIsSuccess || updateIsSuccess || deleteIsSuccess){
      handleCloseFormModal()
      handleCloseConfirmDelete()
    } 
  }, [storeIsSuccess,updateIsSuccess,deleteIsSuccess])
  
  const formik = useFormik({
    initialValues: {
     id: '',
     kode: '',
     nama: '',
     alamat: '',
    },
    validationSchema: Yup.object().shape({
     nama: Yup.string()
     .required(),
     kode: Yup.string()
     .min(3)
     .required(),
     alamat: Yup.string()
    }),
    onSubmit: values => {
      if(!values?.id || values?.id == '' || values?.id == null){
        storePerusahaan({
         nama : values.nama,
         kode : values.kode,
         alamat : values.alamat,
        })
      } else {
        updatePerusahaan({
          id : values.id,
          nama : values.nama,
          kode : values.kode,
          alamat : values.alamat,
        })
      }
    }
  });
  
  const handleCloseConfirmDelete = () => {
    setSelectedRow(null)
    setshowConfirmDeleteModal(false)
  }
  const handleShowConfirmDeleteModal = (row) => {
    setSelectedRow(row)
    setshowConfirmDeleteModal(true)
  }
  const handleDelete = () =>{
    deletePerusahaan({id:selectedRow.id})
  }
  
  const handleCloseFormModal = () => {
    setSelectedRow(null)
    setshowFormModal(false)
    formik.resetForm()
  }
  const handleShowFormModal = (row=null) => {
    setSelectedRow(row)
    setshowFormModal(true)
    if(row){
      Object.keys(row)?.map((key) =>{
        formik.setFieldValue(`${key}`,row[key])
        setTimeout(() => formik.setFieldTouched(`${key}`, true))
      });
    }
  }

  const renderTable = () => {
    const totalRows = perusahaan?.data?.count || 0
    const dataColumns = [
      {
         name: 'No.',
         width: "5%",      
         center:true,
         cell:(row,index) => ((page-1) * perPage)+(index+1)
     },
     {
         name: 'Kode',
         selector: row => row.kode,
         sortable: true,
         sortField: 'kode'
     },
     {
         name: 'Nama',
         selector: row => row.nama,
         sortable: true,
         sortField: 'nama'
     },
     {
         name: 'Alamat',
         selector: row => row.alamat,
     },
     {
       center:true,
       cell:(row) => (
         <div className="btn-group " role="group" aria-label="Basic outlined example">
           <button type="button" onClick={()=>handleShowFormModal(row)} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon="fa-regular fa-pen-to-square" /></button>
           <button type="button" onClick={()=>handleShowConfirmDeleteModal(row)} className="btn btn-sm btn-outline-danger"><FontAwesomeIcon icon="fa-regular fa-trash-can" /></button>
         </div>
       ),
       ignoreRowClick: true,
       allowOverflow: true,
       button: true,
     },
    ];
    return <Table 
    columns={dataColumns} 
    data={perusahaan?.data?.rows || []} 
    totalRows={totalRows} 
    loading={isLoading} 
    setPerPage={setPerPage}
    setPage={setPage}
    setKeyword={setKeyword}
    setOrder={setOrder}
    subHeader={<button type="button" onClick={()=>handleShowFormModal()} className="btn btn-secondary"><FontAwesomeIcon icon="fa-solid fa-plus" /> Add New Data</button>}/>
  }

  return (
    <>
    <div>
        <h1><FontAwesomeIcon icon="fa-solid fa-boxes-stacked"/> Perusahaan</h1>
        <div className="card my-4">
            <div className="card-header d-flex justify-content-between">
              <div style={{lineHeight:'2rem'}}>
              <FontAwesomeIcon icon="fa-solid fa-table" className="me-1"/>
              Data Perusahaan
              </div>
              <div className="">
                <CSVDownloadButton filename={`perusahaan`} url="perusahaan/download/csv"/>
              </div>
            </div>
            <div className="card-body">
                {renderTable()}
            </div>
        </div>

        <Modal show={showConfirmDeleteModal} onHide={handleCloseConfirmDelete} backdrop="static" keyboard={false}>
          <Modal.Header closeButton>
            <Modal.Title>Confirm Delete</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure want to delete <b>{selectedRow?.nama}</b> ?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseConfirmDelete}>
              Close
            </Button>
            <Button variant="danger" onClick={handleDelete}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={showFormModal} onHide={handleCloseFormModal} backdrop="static" keyboard={false}>
          <form onSubmit={formik.handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>Perusahaan</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <>
                  <div className="mb-3">
                    <label htmlFor="kode" className="form-label">Kode</label>
                    <input
                      name="kode"
                      className={`form-control ${formik.errors.kode && formik.touched.kode ? `is-invalid` : formik.values.kode && 'is-valid'}`}
                      id="kode"
                      type="text"
                      placeholder="Kode Perusahaan"
                      {...formik.getFieldProps('kode')}
                      />
                      {formik.errors.kode && formik.touched.kode && (<i className="invalid-feedback">{formik.errors.kode}</i>)}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="nama" className="form-label">Nama</label>
                    <input
                      name="nama"
                      className={`form-control ${formik.errors.nama && formik.touched.nama ? `is-invalid` : formik.values.nama && 'is-valid'}`}
                      id="nama"
                      type="text"
                      placeholder="Nama Perusahaan"
                      {...formik.getFieldProps('nama')}
                      />
                      {formik.errors.nama && formik.touched.nama && (<i className="invalid-feedback">{formik.errors.nama}</i>)}
                  </div>
                  <div className="mb-3">
                    <label htmlFor="alamat" className="form-label">Alamat</label>
                      <textarea 
                      className={`form-control ${formik.errors.alamat && formik.touched.alamat ? `is-invalid` : formik.values.alamat && 'is-valid'}`}
                      id="alamat"
                      name="alamat"
                      rows="3"
                      {...formik.getFieldProps('alamat')}
                      ></textarea>
                      {formik.errors.alamat && formik.touched.alamat && (<i className="invalid-feedback">{formik.errors.alamat}</i>)}
                  </div>
                </>
            </Modal.Body>
            <Modal.Footer>
              <button onClick={handleCloseFormModal} type="button" className="btn btn-secondary">Close</button>
              <button type="submit" className="btn btn-primary" disabled={!(formik.isValid)}>Save</button>
            </Modal.Footer>
          </form>
        </Modal>
    </div>
    </>
  )
}
