import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Formik, Form, Field } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from 'yup';
import { useAuth } from "../../hooks/useAuth"
 
export const Login = () => {
 const {login,auth} = useAuth();

 const LoginSchema = Yup.object().shape({
   username: Yup.string()
   .min(2)
   .max(50)
   .required(),
   password: Yup.string()
   .min(2)
   .max(50)
   .required(),
 });

 const handleLogin = (values) => {
    login(values)
 }
 
 return (
  <div className="bg-dark">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-5">
                            <div className="card shadow-lg border-0 rounded-lg mt-5">
                                <div className="card-header">
                                    <h3 className="text-center font-weight-light my-4">Ifabula CMS</h3>
                                </div>
                                <div className="card-body">
                                    {auth?.isError && auth?.message && (
                                        <div className="alert alert-danger d-flex align-items-center" role="alert">
                                            <FontAwesomeIcon icon="fa-solid fa-triangle-exclamation" className="me-2" />
                                            <div> {auth?.message} </div>
                                        </div>
                                    )}
                                   
                                   <Formik
                                     initialValues={{
                                       username: '',
                                       password: ''
                                     }}
                                     validationSchema={LoginSchema}
                                     onSubmit={values => {
                                      handleLogin(values)
                                     }}
                                   >
                                    {({ errors, touched }) => (
                                     <Form>
                                         <div className="form-floating mb-3">
                                             <Field
                                              name="username"
                                              className={`form-control ${errors.username && touched.username && (`is-invalid`)}`}
                                              id="username"
                                              type="text"
                                              placeholder="username"
                                             />
                                             <label htmlFor="username">Username</label>
                                             {errors.username && touched.username && (<i className="invalid-feedback">{errors.username}</i>)}
                                         </div>
                                         <div className="form-floating mb-3">
                                             <Field
                                              name="password"
                                              className={`form-control ${errors.password && touched.password && (`is-invalid`)}`}
                                              id="password"
                                              type="password"
                                              placeholder="Password"
                                             />
                                             <label htmlFor="password">Password</label>
                                             {errors.password && touched.password && (<i className="invalid-feedback">{errors.password}</i>)}
                                         </div>
                                         
                                         <div className="d-grid gap-2">
                                             <button className="btn btn-dark" type="submit">Login</button>
                                         </div>
                                     </Form>
                                    )}
                                   </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
        <div id="layoutAuthentication_footer">
            <footer className="py-4 bg-light mt-auto">
                <div className="container-fluid px-4">
                    <div className="d-flex align-items-center justify-content-between small">
                        <div className="text-muted">Copyright &copy; Ifabula CMS by Amir Mufid</div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
  </div>
 );
};
