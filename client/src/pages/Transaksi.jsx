import { useEffect, useState } from "react"
import { useFormik } from "formik";
import * as Yup from "yup"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { format as currency } from "currency-formatter"
import { Button, Modal } from "react-bootstrap"

import { useGetTransaksiQuery, useStoreTransaksiMutation, useUpdateTransaksiMutation, useDeleteTransaksiMutation  } from "../redux/services/TransaksiApi"

import { useGetPerusahaanQuery } from "../redux/services/PerusahaanApi"
import { useGetBarangQuery } from "../redux/services/BarangApi"

import Table from "../components/Table"
import CSVDownloadButton from "../components/CSVDownloadButton";
import moment from "moment";
import Autocomplete from "../components/Autocomplete";
export const Transaksi = () => {
  const [showConfirmDeleteModal, setshowConfirmDeleteModal] = useState(false)
  const [showFormModal, setshowFormModal] = useState(false)
  const [selectedRow, setSelectedRow] = useState(null)
  const [perPage,setPerPage] = useState(10)
  const [page,setPage] = useState(1)
  const [order,setOrder] = useState( [] )
  const [keyword,setKeyword] = useState('')
  const [namaPerusahaan,setNamaPerusahaan] = useState('')
  const [namaBarang,setNamaBarang] = useState('')
  
  const {isError,isSuccess,isLoading,data:transaksi} = useGetTransaksiQuery({
    per_page : perPage,
    page,
    order,
    keyword,
  });
  
  const {data:autocomplete_perusahaan} = useGetPerusahaanQuery({
    per_page : 5,
    page : 1,
    order : ['nama','asc'],
    keyword:namaPerusahaan,
  });
  
  const {data:autocomplete_barang} = useGetBarangQuery({
    per_page : 5,
    page : 1,
    order : ['nama','asc'],
    keyword:namaBarang,
  });
  
  const [storeTransaksi,{isSuccess:storeIsSuccess}] = useStoreTransaksiMutation();
  const [updateTransaksi,{isSuccess:updateIsSuccess}] = useUpdateTransaksiMutation();
  const [deleteTransaksi,{isSuccess:deleteIsSuccess}] = useDeleteTransaksiMutation();
  
  useEffect(() => {
    if(storeIsSuccess || updateIsSuccess || deleteIsSuccess){
      handleCloseFormModal()
      handleCloseConfirmDelete()
    } 
  }, [storeIsSuccess,updateIsSuccess,deleteIsSuccess])
  
  const formik = useFormik({
    initialValues: {
      id: '',
      perusahaan_id: '',
      barang_id: '',
      qty: '',
    },
    validationSchema: Yup.object().shape({
      barang_id: Yup.string().uuid().required(),
      perusahaan_id: Yup.string().uuid().required(),
      qty: Yup.number().min(1).required(),
    }),
    onSubmit: values => {
      if(!values?.id || values?.id == '' || values?.id == null){
        storeTransaksi({
          barang_id : values.barang_id,
          perusahaan_id : values.perusahaan_id,
          qty : values.qty
        })
      } else {
        updateTransaksi({
          id : values.id,
          barang_id : values.barang_id,
          perusahaan_id : values.perusahaan_id,
          qty : values.qty
        })
      }
    }
  });
  
  const handleCloseConfirmDelete = () => {
    setSelectedRow({})
    setshowConfirmDeleteModal(false)
  }
  const handleShowConfirmDeleteModal = (row) => {
    setSelectedRow(row)
    setshowConfirmDeleteModal(true)
    
    setNamaPerusahaan('')
    setNamaBarang('')
  }
  const handleDelete = () =>{
    deleteTransaksi({id:selectedRow.id})
  }
  
  const handleCloseFormModal = () => {
    setSelectedRow(null)
    setshowFormModal(false)
    formik.resetForm()
  }
  const handleShowFormModal = (row=null) => {
    setSelectedRow(row)
    setshowFormModal(true)

    const perusahaan_id = row ? row['perusahaan.id'] : '';
    const barang_id = row ? row['barang.id'] : '';

    formik.setFieldValue(`perusahaan_id`,perusahaan_id)
    formik.setFieldValue(`barang_id`,barang_id)
    formik.setFieldValue(`qty`,row?.qty)

    const a = row ? `${row ? row['barang.nama'] : ''} | Stock : ${row ? row['barang.stock'] : ''}` : '';
    const b = row ? `${row ? row['perusahaan.kode'] : ''} | ${row ? row['perusahaan.nama'] : ''}` : '';
    setNamaBarang(a)
    setNamaPerusahaan(b)

    if(row){
      Object.keys(row)?.map((key) =>{
        formik.setFieldValue(`${key}`,row[key])
        setTimeout(() => formik.setFieldTouched(`${key}`, true))
      });
    }
  }

  const renderTable = () => {
    const totalRows = transaksi?.data?.count || 0
    const dataColumns = [
      {
        name: 'No.',
        width: "5%",      
        center:true,
        cell:(row,index) => ((page-1) * perPage)+(index+1)
      },
      {
          name: 'Tanggal Input',
          selector: row => moment(row.createdAt).format('D MMM YYYY hh:mm:ss'),
          center:true,
          sortable: true,
          sortField: 'createdAt'
      },
      {
          name: 'Nama Perusahaan',
          selector: row => row['perusahaan.nama'],
          sortable: true,
          sortField: 'perusahaan.nama'
      },
      {
          name: 'Nama Barang',
          selector: row => row['barang.nama'],
          sortable: true,
          sortField: 'barang.nama',
      },
      {
          name: 'Total Barang',
          selector: row => row.qty,
          center:true,
          sortable: true,
          sortField: 'qty'
      },
      {
          name: 'Harga Barang',
          selector: row => currency(row['barang.harga'], {code:'IDR'}),
          center:true,
          sortable: true,
          sortField: 'barang.harga'
      },
      {
          name: 'Grand Total',
          selector: row => currency(row.total_harga, {code:'IDR'}),
          center:true,
          sortable: true,
          sortField: 'total_harga'
      },
      {
          name: 'Sisa Barang',
          selector: row => row['barang.stock'],
          center:true,
          sortable: true,
          sortField: 'barang.stock'
      },
      {
        center:true,
        cell:(row) => (
          <div className="btn-group " role="group" aria-label="Basic outlined example">
            <button type="button" onClick={()=>handleShowFormModal(row)} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon="fa-regular fa-pen-to-square" /></button>
            <button type="button" onClick={()=>handleShowConfirmDeleteModal(row)} className="btn btn-sm btn-outline-danger"><FontAwesomeIcon icon="fa-regular fa-trash-can" /></button>
          </div>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
    ];
    return <Table 
      columns={dataColumns} 
      data={transaksi?.data?.rows || []} 
      totalRows={totalRows} 
      loading={isLoading} 
      setPerPage={setPerPage}
      setPage={setPage}
      setKeyword={setKeyword}
      setOrder={setOrder}
      subHeader={<button type="button" onClick={()=>handleShowFormModal()} className="btn btn-secondary"><FontAwesomeIcon icon="fa-solid fa-plus" /> Add New Data</button>}
    />
  }

  return (
    <>
    <div>
        <h1><FontAwesomeIcon icon="fa-solid fa-boxes-stacked"/> Transaksi</h1>
        <div className="card my-4">
            <div className="card-header d-flex justify-content-between">
              <div style={{lineHeight:'2rem'}}>
              <FontAwesomeIcon icon="fa-solid fa-table" className="me-1"/>
              Data Transaksi
              </div>
              <div className="">
                <CSVDownloadButton filename={`transaksi`} url="transaksi/download/csv"/>
              </div>
            </div>
            <div className="card-body">
                {renderTable()}
            </div>
        </div>

        <Modal show={showConfirmDeleteModal} onHide={handleCloseConfirmDelete} backdrop="static" keyboard={false}>
          <Modal.Header closeButton>
            <Modal.Title>Confirm Delete</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure want to delete <b>{selectedRow?.nama}</b> ?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleCloseConfirmDelete}>
              Close
            </Button>
            <Button variant="danger" onClick={handleDelete}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>

        <Modal show={showFormModal} onHide={handleCloseFormModal} backdrop="static" keyboard={false}>
          <form onSubmit={formik.handleSubmit}>
            <Modal.Header closeButton>
              <Modal.Title>Transaksi</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <>
                  <div className="mb-3">
                    <label htmlFor="nama" className="form-label">Perusahaan</label>
                    <Autocomplete 
                      name="perusahaan_id" 
                      data={autocomplete_perusahaan?.data} 
                      onKeyUp={(val)=>setNamaPerusahaan(val)} 
                      text={(row)=>`${row.kode} | ${row.nama}`} 
                      setValueSuggestion={(row)=>row.id} 
                      defaultValue={namaPerusahaan}
                      formik={formik}
                      />
                  </div>
                  <div className="row">
                    <div className="col">
                      <label htmlFor="harga" className="form-label">Barang</label>
                      <Autocomplete 
                        name="barang_id" 
                        data={autocomplete_barang?.data} 
                        onKeyUp={(val)=>setNamaBarang(val)} 
                        text={(row)=>`${row.nama} | Stock : ${row.stock}`} 
                        setValueSuggestion={(row)=>row.id} 
                        defaultValue={namaBarang}
                        formik={formik}
                      />
                    </div>
                    <div className="col">
                      <label htmlFor="qty" className="form-label">Qty</label>
                      <input
                        name="qty"
                        className={`form-control ${formik.errors.qty && formik.touched.qty ? `is-invalid` : formik.values.qty && 'is-valid'}`}
                        id="qty"
                        type="text"
                        placeholder="Qty"
                        {...formik.getFieldProps('qty')}
                        />
                        {formik.errors.qty && formik.touched.qty && (<i className="invalid-feedback">{formik.errors.qty}</i>)}
                    </div>
                  </div>
                </>
            </Modal.Body>
            <Modal.Footer>
              <button onClick={handleCloseFormModal} type="button" className="btn btn-secondary">Close</button>
              <button type="submit" className="btn btn-primary" disabled={!(formik.isValid)}>Save</button>
            </Modal.Footer>
          </form>
        </Modal>
    </div>
    </>
  )
}
