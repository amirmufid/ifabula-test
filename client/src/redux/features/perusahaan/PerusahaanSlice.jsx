import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { PerusahaanAPI } from "../../api";

//GET
export const getPerusahaan = createAsyncThunk('Perusahaan/getData', async (params, {rejectWithValue}) => {
  try {
   const data = await PerusahaanAPI.get(params);
   return data.status == 200 ? data.data : []
  } catch (err){
   return rejectWithValue(err.response.data)
  }
})

//INSERT
export const storePerusahaan = createAsyncThunk('Perusahaan/storeData', async (params, {rejectWithValue}) => {
 try {
  const data = await PerusahaanAPI.store(params);
  return data.status == 201 ? data.data : []
 } catch (err){
  return rejectWithValue(err.response.data)
 }
})

//UPDATE
export const updatePerusahaan = createAsyncThunk('Perusahaan/updateData', async (params, {rejectWithValue}) => {
 try {
  const data = await PerusahaanAPI.update(params.id,params.updateValue);
  return data.status == 201 ? data.data : []
 } catch (err){
  return rejectWithValue(err.response.data)
 }
})

//DELETE
export const deletePerusahaan = createAsyncThunk('Perusahaan/deleteData', async ({id}, {rejectWithValue}) => {
 try {
  const data = await PerusahaanAPI.delete(id);
  return data.status == 201 ? data.data : []
 } catch (err){
  return rejectWithValue(err.response.data)
 }
})

const initialState = {
  data : [],
  isSuccess : false,
  message : "",
  loading : false,
  insertAction : {
   isSuccess : false,
   message : "",
   data: {},
   loading : false,
  },
  updateAction : {
   isSuccess : false,
   message : "",
   data: {},
   loading : false,
  },
  deleteAction : {
   isSuccess : false,
   message : "",
   loading : false,
  }
 }

const PerusahaanSlice = createSlice({
 name : "Perusahaan",
 initialState,
 reducers : {},
 extraReducers: {
  //GET
  [getPerusahaan.pending]:(state,{payload}) => {
   state.loading = true
  },
  [getPerusahaan.fulfilled]:(state,{payload}) => {
   state.isSuccess = !payload?.error
   state.loading = false
   state.data = payload?.data
   state.message = payload?.message
  },
  [getPerusahaan.rejected]:(state,{payload}) => {
   state.isSuccess = !payload?.error
   state.loading = false
   state.message = payload?.message
  },

  //INSERT
  [storePerusahaan.pending]:(state,{payload}) => {
   state.insertAction.loading = true
  },
  [storePerusahaan.fulfilled]:(state,{payload}) => {
   state.insertAction.isSuccess = !payload?.error
   state.insertAction.loading = false
   state.insertAction.data = payload?.data
   state.insertAction.message = payload?.message
  },
  [storePerusahaan.rejected]:(state,{payload}) => {
   state.insertAction.isSuccess = !payload?.error
   state.insertAction.loading = false
   state.insertAction.message = payload?.message
  },

  //UPDATE
  [updatePerusahaan.pending]:(state,{payload}) => {
   state.updateAction.loading = true
  },
  [updatePerusahaan.fulfilled]:(state,{payload}) => {
   state.updateAction.isSuccess = !payload?.error
   state.updateAction.loading = false
   state.updateAction.data = payload?.data
   state.updateAction.message = payload?.message
  },
  [updatePerusahaan.rejected]:(state,{payload}) => {
   state.updateAction.isSuccess = !payload?.error
   state.updateAction.loading = false
   state.updateAction.message = payload?.message
  },

  //DELETE
  [deletePerusahaan.pending]:(state,{payload}) => {
   state.deleteAction.loading = true
  },
  [deletePerusahaan.fulfilled]:(state,{payload}) => {
   state.deleteAction.isSuccess = !payload?.error
   state.deleteAction.loading = false
   state.deleteAction.message = payload?.message
  },
  [deletePerusahaan.rejected]:(state,{payload}) => {
   state.deleteAction.isSuccess = !payload?.error
   state.deleteAction.loading = false
   state.deleteAction.message = payload?.message
  }
 }
})

export default PerusahaanSlice.reducer
