import axios from "axios";
export const getToken = () => {
 let token = null;
 const loginStorage = localStorage.getItem("login") != null ? JSON.parse(localStorage.getItem("login")) : null
 try {
   token = loginStorage.token;
 } catch (error) {
   token = null;
 }
 return token;
};

const API = axios.create({ 
 baseURL: env.API_URL,
 timeout: 5000
});

class Barang {
 get = (params) => API.get(`/barang`,{params,
  headers: {Authorization: `Bearer ${getToken()}`}
 });
 getById = (id) => API.get(`/barang/${id}`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 store = (params) => API.post(`/barang/`,params,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 update = (id,params) => API.put(`/barang/${id}`,params,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 delete = (id) => API.delete(`/barang/${id}`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 csv = () => API.get(`/barang/download/csv`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
}

class Perusahaan {
 get = (params) => API.get(`/perusahaan`,{params,
  headers: {Authorization: `Bearer ${getToken()}`}
 });
 getById = (id) => API.get(`/perusahaan/${id}`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 store = (params) => API.post(`/perusahaan/`,params,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 update = (id,params) => API.put(`/perusahaan/${id}`,params,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 delete = (id) => API.delete(`/perusahaan/${id}`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
 csv = () => API.get(`/perusahaan/download/csv`,
  {headers: {Authorization: `Bearer ${getToken()}`}
 });
}

class Auth {
 login = (params) => API.post(`/auth/login`,params);
}

export const BarangAPI = new Barang()
export const AuthAPI = new Auth()
export const PerusahaanAPI = new Perusahaan()
