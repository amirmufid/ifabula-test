import { Api } from "./Api"
import { serialize } from "../../../utility"

export const BarangApi = Api.injectEndpoints({
 reducerPath: 'barang',
 endpoints: (builder) => ({
  getBarang: builder.query({
   query: (params) => ({
    url : `barang?${serialize(params)}`
   }),
   providesTags: [ 'Barang' ]
  }),
  storeBarang: builder.mutation({
    query: (body) => ({
     url :`barang`,
     method : 'POST',
     body
    }),
    invalidatesTags: ['Barang']
  }),
  updateBarang: builder.mutation({
    query: (body) => ({
     url :`barang/${body.id}`,
     method : 'PUT',
     body : {
      nama : body.nama,
      harga : body.harga,
      stock : body.stock,
     }
    }),
    invalidatesTags: ['Barang']
  }),
  deleteBarang: builder.mutation({
    query: (body) => ({
     url :`barang/${body.id}`,
     method : 'DELETE'
    }),
    invalidatesTags: ['Barang']
  })
 }),
})

export const {useGetBarangQuery, useStoreBarangMutation, useUpdateBarangMutation, useDeleteBarangMutation} = BarangApi
