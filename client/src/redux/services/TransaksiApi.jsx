import { Api } from "./Api"
import { serialize } from "../../../utility"

export const TransaksiApi = Api.injectEndpoints({
 reducerPath: 'transaksi',
 endpoints: (builder) => ({
  getTransaksi: builder.query({
   query: (params) => ({
    url : `transaksi?${serialize(params)}`
   }),
   providesTags: [ 'Transaksi' ]
  }),
  storeTransaksi: builder.mutation({
    query: (body) => ({
     url :`transaksi`,
     method : 'POST',
     body : {
      perusahaan_id : body.perusahaan_id,
      barang_id : body.barang_id,
      qty : body.qty,
     }
    }),
    invalidatesTags: ['Transaksi']
  }),
  updateTransaksi: builder.mutation({
    query: (body) => ({
     url :`transaksi/${body.id}`,
     method : 'PUT',
     body : {
      perusahaan_id : body.perusahaan_id,
      barang_id : body.barang_id,
      qty : body.qty,
     }
    }),
    invalidatesTags: ['Transaksi']
  }),
  deleteTransaksi: builder.mutation({
    query: (body) => ({
     url :`transaksi/${body.id}`,
     method : 'DELETE'
    }),
    invalidatesTags: ['Transaksi']
  })
 }),
})

export const {useGetTransaksiQuery, useStoreTransaksiMutation, useUpdateTransaksiMutation, useDeleteTransaksiMutation} = TransaksiApi
