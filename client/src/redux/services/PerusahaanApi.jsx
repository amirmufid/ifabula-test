import { Api } from "./Api"
import { serialize } from "../../../utility"

export const PerusahaanApi = Api.injectEndpoints({
 reducerPath: 'perusahaan',
 endpoints: (builder) => ({
  getPerusahaan: builder.query({
   query: (params) => ({
    url : `perusahaan?${serialize(params)}`
   }),
   providesTags: [ 'Perusahaan' ]
  }),
  storePerusahaan: builder.mutation({
    query: (body) => ({
     url :`perusahaan`,
     method : 'POST',
     body
    }),
    invalidatesTags: ['Perusahaan']
  }),
  updatePerusahaan: builder.mutation({
    query: (body) => ({
     url :`perusahaan/${body.id}`,
     method : 'PUT',
     body : {
      nama : body.nama,
      kode : body.kode,
      alamat : body.alamat,
     }
    }),
    invalidatesTags: ['Perusahaan']
  }),
  deletePerusahaan: builder.mutation({
    query: (body) => ({
     url :`perusahaan/${body.id}`,
     method : 'DELETE'
    }),
    invalidatesTags: ['Perusahaan']
  })
 }),
})

export const {useGetPerusahaanQuery, useStorePerusahaanMutation, useUpdatePerusahaanMutation, useDeletePerusahaanMutation} = PerusahaanApi
