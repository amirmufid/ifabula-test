import { Api } from "./Api"

export const AuthApi = Api.injectEndpoints({
 reducerPath: 'login',
 endpoints: (builder) => ({
   authLogin: builder.mutation({
    query: (body) => ({
     url :`auth/login`,
     method : 'POST',
     body
    })
  }),
 }),
})

export const {useAuthLoginMutation} = AuthApi
