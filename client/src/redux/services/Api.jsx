import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { useLocalStorage } from '../../../hooks/useLocalStorage';

export const Api = createApi({
 reducerPath: 'api',
 baseQuery: fetchBaseQuery({ 
  baseUrl: env.API_URL,
  prepareHeaders: (headers) => {
   const loginStorage = localStorage.getItem("login") != null ? JSON.parse(localStorage.getItem("login")) : null
   const token = loginStorage?.accessToken?.token || null;
   if (token) {
     headers.set('Authorization', `Bearer ${token}`)
   }
   return headers
 },
 }),
 endpoints: () => ({}),
})
