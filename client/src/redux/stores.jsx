import { configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import { AuthApi } from './services/AuthApi';
import { BarangApi } from './services/BarangApi';
import { PerusahaanApi } from './services/PerusahaanApi';

const store = configureStore({
  reducer: {
    [AuthApi.reducerPath] : AuthApi.reducer,
    [BarangApi.reducerPath] : BarangApi.reducer,
    [PerusahaanApi.reducerPath] : PerusahaanApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false, })
    .concat(AuthApi.middleware)
    .concat(BarangApi.middleware)
    .concat(PerusahaanApi.middleware)
  ,
})

setupListeners(store.dispatch)


export default store;
