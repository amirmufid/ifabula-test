import { Routes, Route, createBrowserRouter, createRoutesFromElements, defer, RouterProvider } from "react-router-dom"

import * as Layout from "./src/components/layouts/Layout";
import { AdminLayout } from "./src/components/layouts/AdminLayout";

import { Dashboard } from "./src/pages/Dashboard";
import { Login } from "./src/pages/Login";
import { Barang } from "./src/pages/Barang";
import { Perusahaan } from "./src/pages/Perusahaan";
import { Transaksi } from "./src/pages/Transaksi";

import ErrorPage from "./src/components/ErrorPage";

const getUserData = () => new Promise((resolve) =>
  setTimeout(() => {
    const user = window.localStorage.getItem("user");
    resolve(user);
  }, 3000)
);
const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route element={<Layout.Root />}
        loader={()=>defer({userPromise : getUserData()})}
        errorElement={<ErrorPage />}
      >
        <Route element={<Layout.NoAuth />}>
          <Route path="/login" element={<Login />} />
        </Route>

        <Route element={<Layout.Auth />}>
          <Route element={<AdminLayout />}>
            <Route path="/" element={<Dashboard />} />
            <Route path="/barang" element={<Barang />} />
            <Route path="/perusahaan" element={<Perusahaan />} />
            <Route path="/transaksi" element={<Transaksi />} />
          </Route>
        </Route>
      </Route>
    </>
  )
);

const App = () => {
  return (
    <RouterProvider router={router} />
  )
}

export default App;
