import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "./useLocalStorage";
import { useEffect } from "react";
import { useAuthLoginMutation } from "../src/redux/services/AuthApi";

export const useAuth = () => {
 const [authLogin,authData] = useAuthLoginMutation();

 const [auth, setAuth] = useLocalStorage("login", null);
 const navigate = useNavigate();

 useEffect(() => {
  if(authData?.isSuccess){
   setAuth({
      isLoading   : authData?.isLoading,
      isSuccess   : authData?.isSuccess,
      isError     : authData?.isError,
      message     : authData?.error?.data?.message,
      accessToken : authData?.data?.access_token,
      data        : authData?.data?.data
   });
   navigate("/");
  }
  
 }, [authData])
 
 useEffect(() => {
   if(!auth?.isSuccess){
    navigate("/login");
   }
   
  }, [auth])
 

 const login = async (values) => {
  authLogin({
     username : values.username,
     password : values.password
  })
 }

 const logout = async () => {
   setAuth(null);
   navigate("/login",{ replace:true });
 }

 return {
  login,logout,auth
 };
};
