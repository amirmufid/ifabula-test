'use strict';
const bcrypt = require('bcrypt')
const { v4: uuid } = require('uuid');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync('1234', salt);

    return queryInterface.bulkInsert('users', [{
      id: uuid(),
      username: 'admin',
      password: hash,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);

  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('users', null, {});

  }
};
