const Response = async function (req, res, next) {
    if (req._status == undefined) {
        res.status(500).json({
            data: [],
            message: 'response status undefined'
        })
    } else {
        
        let status = req._status || 400
        let response = {}
        response = req._response || {}
        response.error = req._error || false  
        response.access_key=req._access_key      
        if(req._message) response.message = req._message

        let data = req._data == undefined ? null : req._data
        if(!response.error) response.data = data
        
        res.status(status).json(response)
    }
}

module.exports = Response
