const jwt = require('jsonwebtoken')
const config = require('../config')
const {users} = require('../models')

const Auth = (req, res, next) => {
 const authorization = req.headers["authorization"] || " ";
 const token = authorization?.split(" ");
  if(token[0] !== 'Bearer' || !token[1] || token[1]==''){
    return res.status(401).json({
      error: true,
      message: "UNAUTHORIZED EMPTY TOKEN"
    });
  }
  
  jwt.verify(token[1], config.JWT_SECRET_KEY, (err, decoded) => {
    if (err) {
      return res.status(401).json({
        error: true,
        message: "UNAUTHORIZED INVALID TOKEN"
      });
    }
    req.user_data = decoded.data;
    checkUserExist(decoded.data?.id, res, next);
  });
};

const checkUserExist = async (id, res, next) => {
  try {
    const user = await users.count({ where: { id } });
    if (user > 0) {
      next();
    } else {
      return res.status(401).json({
        error: true,
        message: "UNAUTHORIZED USER NOT FOUND"
      });
    }
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: error?.message
    });
  }
};

module.exports = Auth
