const lodash = require('lodash')

module.exports = (paramSchema) => {
    return async (req, res, next) => {
        try {
          let param = req?.body || null
            if(req.method == "GET"){
             param = req?.query || null
            }
            await paramSchema.validateAsync(param);
        } catch (err) {
            return res.send(400, {
                error: true,
                message: err.message
            });
        }
        next();
    }
};
