const Joi = require('joi')

const getSchema = Joi.object({
 per_page: Joi.number().min(0).allow(null,''),
 page: Joi.number().min(0).allow(null,''),
 order : Joi.array().items(Joi.string().allow(null,'')).allow(null,''),
 keyword: Joi.string().allow(null,'')
});


const storeSchema = Joi.object({
 nama: Joi.string().required(),
 kode: Joi.string().min(3).required(),
 alamat: Joi.string().required()
});

module.exports = {getSchema,storeSchema}
