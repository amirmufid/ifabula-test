'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('transaksi', {
      id:  {
        type: Sequelize.DataTypes.UUID,
        primaryKey:true
      },
      perusahaan_id: {
        type: Sequelize.DataTypes.STRING,
        defaultValue: false,
        allowNull: false
      },
      barang_id: {
        type: Sequelize.DataTypes.TEXT,
        defaultValue: false,
        allowNull: true
      },
      qty: {
        type: Sequelize.DataTypes.INTEGER,
        defaultValue: false,
        allowNull: true
      },
      harga: {
        type: Sequelize.DataTypes.INTEGER,
        defaultValue: false,
        allowNull: true
      },
      total_harga: {
        type: Sequelize.DataTypes.INTEGER,
        defaultValue: false,
        allowNull: true
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('transaksi');
  }
};
