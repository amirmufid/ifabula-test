'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('perusahaan', {
      id:  {
        type: Sequelize.DataTypes.UUID,
        primaryKey:true
      },
      kode: {
        type: Sequelize.DataTypes.STRING,
        defaultValue: false,
        allowNull: false,
        unique: true
      },
      nama: {
        type: Sequelize.DataTypes.STRING,
        defaultValue: false,
        allowNull: false
      },
      alamat: {
        type: Sequelize.DataTypes.TEXT,
        defaultValue: false,
        allowNull: true
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('perusahaan');
  }
};
