'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.createTable('barang', {
      id:  {
        type: Sequelize.DataTypes.UUID,
        primaryKey:true
      },
      nama: {
        type: Sequelize.DataTypes.STRING,
        defaultValue: false,
        allowNull: false
      },
      harga: {
        type: Sequelize.DataTypes.DOUBLE,
        defaultValue: 0,
        allowNull: false
      },
      stock: {
        type: Sequelize.DataTypes.INTEGER,
        defaultValue: 0,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DataTypes.DATE
      },
      updatedAt: {
        type: Sequelize.DataTypes.DATE
      }
    });
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.dropTable('barang');
  }
};
