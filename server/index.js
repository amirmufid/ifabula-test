const express = require('express')
const cluster = require('cluster')
const cors = require("cors");
const bodyParser = require('body-parser');
const multer = require('multer');

const config = require('./config')
const routes = require('./routes')


const allowedOrigins = [config.FE_BASEURL,config.BASE_URL];

const app = express();
const upload = multer();

app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(upload.array()); 

app.use(cors({
 origin: function(origin, callback){
   if(!origin) return callback(null, true);
   if(allowedOrigins.indexOf(origin) == -1){
     var msg = 'The CORS policy for this site does not ' +
               'allow access from the specified Origin.';
     return callback(msg, false);
   }
   return callback(null, true);
 }
}));

app.get("/", (req, res, next) => {
 res.send(`server running on pid ${process.pid} port ${config.PORT}, https: ${config.USE_HTTPS}`);
});

app.use("/api", routes);

app.use((req, res,next)=>{
 res.status(404).send('<h1> Page not found </h1>');
});

const CLUSTER = config.CLUSTER_ENABLE === 'true' ? true : false;
if (CLUSTER && cluster.isPrimary) {
 for (let i = 0; i < config.CLUSTER_CPUS; i++) {
   cluster.fork();
 }

 cluster.on('exit', (worker, code, signal) => {
   cluster.fork();
 });
} else {
 let server;
 if (config.USE_HTTPS == "true") {
   server = https.createServer(credentials, app);
 } else {
   server = app;
 }
 
 server.listen(config.PORT, () => {
   console.log(`server running on pid ${process.pid} port ${config.PORT}, https: ${config.USE_HTTPS}`);
 });
}
