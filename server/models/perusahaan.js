const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('perusahaan', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true
    },
    kode: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0",
      unique: "kode"
    },
    nama: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0"
    },
    alamat: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'perusahaan',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "kode",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "kode" },
        ]
      },
    ]
  });
};
