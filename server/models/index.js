'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const config = require(__dirname + '/../config/database.js');
const db = {};

//sequelize connection
const sequelize = new Sequelize(config.database, config.username, config.password, config);

//generate all model in all folder models
fs.readdirSync(__dirname).filter(file => {
  return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
})
.forEach(file => {
  const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
  db[model.name] = model
});

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

//associations
db.barang.hasMany(db.transaksi, { foreignKey: 'barang_id', targetKey: 'id'})
db.transaksi.belongsTo(db.barang, { foreignKey: 'barang_id', targetKey: 'id'});
db.perusahaan.hasMany(db.transaksi, { foreignKey: 'perusahaan_id', targetKey: 'id'})
db.transaksi.belongsTo(db.perusahaan, { foreignKey: 'perusahaan_id', targetKey: 'id'});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
