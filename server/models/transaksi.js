const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('transaksi', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true
    },
    perusahaan_id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: "0"
    },
    barang_id: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    qty: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    harga: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    total_harga: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'transaksi',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
