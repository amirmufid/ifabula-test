const express = require('express')

const {Response} = require('./middleware')

const authRoutes = require('./api/auth/router')
const barangRoutes = require('./api/barang/router')
const perusahaanRoutes = require('./api/perusahaan/router')
const transaksiRoutes = require('./api/transaksi/router')

const router = express.Router()
router.use("/auth", authRoutes, Response)
router.use("/barang", barangRoutes, Response)
router.use("/perusahaan", perusahaanRoutes, Response)
router.use("/transaksi", transaksiRoutes, Response)

module.exports = router
