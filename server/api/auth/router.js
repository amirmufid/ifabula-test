const express = require('express')
const config = require('../../config')
const Controller = require('./controller')
const {Validator} = require('../../middleware')
const {loginSchema} = require('../../middleware/validator/auth.validator')

const router = express.Router()

router.post("/login", [Validator(loginSchema)] , Controller.login)

module.exports = router
