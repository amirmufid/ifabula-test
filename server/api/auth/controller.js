const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {Sequelize,users} = require('../../models');
const config = require('../../config')

class Auth {
 login = async (req, res, next) => {
  try {
   const body = req.body
   const data = await users.findOne({
       where:{ username : body.username }
   })

   let error = false
   if(!data) {
    error = true
   } else {

    const passwordIsValid = bcrypt.compareSync(
      body.password,
      data.password
    );

    if (!passwordIsValid) {
      error = true
    }
  }

   if(!error){
    /* expiry 1 hour */
    const expiryToken = Math.floor(Date.now() / 1000) + 60 * (config.JWT_EXPIRY_TOKEN * 60);

    const token = jwt.sign(
      {
        exp: expiryToken,
        data: data,
      },
      config.JWT_SECRET_KEY
    );

    req._data = data;
    req._response = {
     access_token: {
       token,
       expiryIn: expiryToken,
     }
    };

    req._status = 200
    req._message = `Berhasil Login`
   } else {
    req._error = true
    req._status = 403
    req._message = `UNAUTHORIZED`
   }
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
}

module.exports = new Auth()
