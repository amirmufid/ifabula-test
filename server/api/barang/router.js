const express = require('express')
const config = require('../../config')
const Controller = require('./controller')
const {Auth,Validator} = require('../../middleware')
const {getSchema,storeSchema} = require('../../middleware/validator/barang.validator')

const router = express.Router()

router.get("/", [Auth,Validator(getSchema)] , Controller.get)
router.get("/:id", [Auth] , Controller.getById)
router.post("/", [Auth,Validator(storeSchema)] , Controller.store)
router.put("/:id", [Auth,Validator(storeSchema)] , Controller.update)
router.delete("/:id", [Auth] , Controller.delete)
router.get("/download/:format", [Auth] , Controller.download)

module.exports = router
