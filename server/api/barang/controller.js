const { v4: uuid } = require('uuid');
const {Sequelize,barang} = require('../../models');
const { downloadResource } = require('../../utility');

class Barang {
 get = async (req, res, next) => {
  try {
   let where = {}
   let limit = parseInt(req.query?.per_page) || null
   let offset = (parseInt(req.query?.page)-1)*limit || null
   let keyword = req.query?.keyword || null
   let order = req.query?.order ? [req.query?.order] : null 

   if(keyword){
    where = {
        ...where,
        nama : { [Sequelize.Op.substring] : keyword }
    }
   }
   const data = await barang.findAndCountAll({where,limit,offset,order});

   req._status = 200
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
 
 getById = async (req, res, next) => {
  try {
   const id = req.params.id
   const data = await barang.findOne({
       where:{ id }
   })
   req._status = 200
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
 
 store = async (req, res, next) => {
  try {
   const body = req.body
   const storeData = {
    id : uuid(),
    nama : body.nama,
    harga : body.harga,
    stock : body.stock
   }
   const data = await barang.create(storeData)
   req._status = 201
   req._message = `Berhasil tambah data`
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }

 update = async (req, res, next) => {
   try {
    const id = req.params.id
    const body=req.body
    
    const storeData = {
     id : id,
     nama : body.nama,
     harga : body.harga,
     stock : body.stock
    }
    await barang.update(storeData,{
     where:{ id }
    })
    
    req._data = storeData
    req._status = 201
    req._message = "Berhasil update data"

    next()
   } catch (err) {
    req._status = 500
    req._error = true
    req._message = err.message
   }
 }
 
 delete = async (req, res, next) => {
  try {
   const id = req.params.id
   const data = await barang.destroy({
    where : { id }
   })
   req._status = 201
   req._message = `Berhasil hapus data`
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }

 download = async (req, res, next) => {
  try {
    const fields = [
      {
        label: 'ID',
        value: 'id'
      },
      {
        label: 'Nama',
        value: 'nama'
      },
      {
        label: 'Harga',
        value: 'harga'
      },
      {
        label: 'Stock',
        value: 'stock'
      }
    ];
    const attributes = fields.map((x)=>x.value)
    const data = await barang.findAll({
      attributes
    });
    
    return downloadResource(res, 'barang.csv', fields, data);
  
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
}

module.exports = new Barang()
