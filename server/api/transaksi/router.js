const express = require('express')
const config = require('../../config')
const Controller = require('./controller')
const {Auth,Validator} = require('../../middleware')
const {getSchema,storeSchema} = require('../../middleware/validator/transaksi.validator')

const router = express.Router()

router.get("/", [Validator(getSchema)] , Controller.get)
router.get("/:id", [] , Controller.getById)
router.post("/", [Validator(storeSchema)] , Controller.store)
router.put("/:id", [Validator(storeSchema)] , Controller.update)
router.delete("/:id", [] , Controller.delete)
router.get("/download/:format", [Auth] , Controller.download)

module.exports = router
