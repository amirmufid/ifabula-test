const { v4: uuid } = require('uuid');
const {Sequelize,transaksi,barang,perusahaan} = require('../../models');
const { downloadResource } = require('../../utility');

class Transaksi {
 get = async (req, res, next) => {
  try {
   let where = {}
   let limit = parseInt(req.query?.per_page) || null
   let offset = (parseInt(req.query?.page)-1)*limit || null
   let keyword = req.query?.keyword || null
   let order = req.query?.order ? [req.query?.order] : null 

   if(keyword){
    where = {
        ...where,
        [Sequelize.Op.or] : [
          {
            '$perusahaan.nama$' : { [Sequelize.Op.substring] : keyword }
          },
          {
            '$barang.nama$' : { [Sequelize.Op.substring] : keyword }
          },
        ]
    }
   }
   const data = await transaksi.findAndCountAll({
    include : [{
      model : barang,
      required : true
    },{
      model : perusahaan,
      required : true
    }],
    raw:true,
    where,limit,offset,order});

   req._status = 200
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
 
 getById = async (req, res, next) => {
  try {
   const id = req.params.id
   const data = await transaksi.findOne({
    include : [{
      model : barang,
      required : true
    },{
      model : perusahaan,
      required : true
    }],
    where:{ id }
   })
   req._status = 200
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
 
 store = async (req, res, next) => {
  try {
   const body = req.body
   const barangData = await barang.findOne({
    where : {
      id : body.barang_id
    }
   })

   if(!barangData){
    
    req._status = 400
    req._error = true
    req._message = 'barang_id not found'
    return next()
   }

   const storeData = {
    id : uuid(),
    perusahaan_id : body.perusahaan_id,
    barang_id : body.barang_id,
    qty : body.qty,
    harga : barangData?.harga,
    total_harga : barangData?.harga * body.qty
   }
   const data = await transaksi.create(storeData)
   req._status = 201
   req._message = `Berhasil tambah data`
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }

 update = async (req, res, next) => {
   try {
    const id = req.params.id
    const body=req.body
    
    const barangData = await barang.findOne({
      where : {
        id : body.barang_id
      }
    })
    
    const storeData = {
     id : id,
     perusahaan_id : body.perusahaan_id,
     barang_id : body.barang_id,
     qty : body.qty,
     harga : barangData.harga,
     total_harga : barangData.harga * body.qty
    }
    await transaksi.update(storeData,{
     where:{ id }
    })
    
    req._data = storeData
    req._status = 201
    req._message = "Berhasil update data"

    next()
   } catch (err) {
    req._status = 500
    req._error = true
    req._message = err.message
   }
 }
 
 delete = async (req, res, next) => {
  try {
   const id = req.params.id
   const data = await transaksi.destroy({
    where : { id }
   })
   req._status = 201
   req._message = `Berhasil hapus data`
   req._data = data
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }

 download = async (req, res, next) => {
  try {
    const fields = [
      {
        label: 'Tanggal Input',
        value: 'tanggal_input'
      },
      {
        label: 'Nama Perusahaan',
        value: 'nama_perusahaan'
      },
      {
        label: 'Nama Barang',
        value: 'barang.nama'
      },
      {
        label: 'Total Barang',
        value: 'total_barang'
      },
      {
        label: 'Harga Barang',
        value: 'harga_barang'
      },
      {
        label: 'Grand Total',
        value: 'grand_total'
      },
      {
        label: 'Sisa Barang',
        value: 'sisa_barang'
      }
    ];
    
    const data = await transaksi.findAll({
      include : [{
        attributes:[],
        model : barang,
        required : true
      },{
        attributes:[],
        model : perusahaan,
        required : true
      }],
      attributes : [
        [
          Sequelize.fn(
            "DATE_FORMAT", Sequelize.col("transaksi.createdAt"), "%d-%m-%Y"
          ),
          "tanggal_input",
        ],
        [Sequelize.col("perusahaan.nama"),"nama_perusahaan"],
        [Sequelize.col("barang.nama"),"nama_barang"],
        ['qty','total_barang'],
        [Sequelize.col("barang.harga"),"harga_barang"],
        ["total_harga","grand_total"],
        [Sequelize.col("barang.stock"),"sisa_barang"],
      ],
      raw:true,
    });
    
    return downloadResource(res, 'transaksi.csv', fields, data);
  } catch (err) {
   req._status = 500
   req._error = true
   req._message = err.message
  }
  next()
 }
}

module.exports = new Transaksi()
