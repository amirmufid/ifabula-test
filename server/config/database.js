const process = require('process');
const dotenv = require('dotenv')
const env = process.env.NODE_ENV || 'development';
dotenv.config({ path: `.env.${env}` })
const conf = {
   username: process.env.DB_USER,
   password: process.env.DB_PASSWORD,
   database: process.env.DB_NAME,
   host: process.env.DB_HOST,
   dialect: process.env.DB_DIALECT,
   timezone:'Asia/Jakarta',
   logging: env=='development'?true:false
}

module.exports = conf
