const dotenv = require('dotenv')
const envFile = process.env.NODE_ENV ? `.env.${process.env.NODE_ENV.trim()}` : '.env'
dotenv.config({
 path: envFile
});

const config = {
 NODE_ENV : process.env.NODE_ENV || 'development',
 HOST : process.env.HOST,
 PORT : process.env.PORT,
 FE_BASEURL: process.env.FE_BASEURL,
 USE_HTTPS: process.env.USE_HTTPS || false,
 BASE_URL : `${process.env.USE_HTTPS == 'true' ? 'https':'http'}://${process.env.HOST}:${process.env.PORT}`,
 CLUSTER_ENABLE : process.env.CLUSTER_ENABLE,
 CLUSTER_CPUS : process.env.CLUSTER_CPUS,
 JWT_SECRET_KEY : process.env.JWT_SECRET_KEY || 'secret-key',
 JWT_EXPIRY_TOKEN : process.env.JWT_EXPIRY_TOKEN || 24
}

module.exports = config
